import { MDBCol, MDBContainer, MDBRow } from "mdb-react-ui-kit";
import React, { Component } from "react";

export default class CASDropdown extends Component {
  constructor() {
    super();
    this.state = {
      categories: [],
      subcategories: [],
      selectdropdown: "",
      description: ""
    };

    this.selectDropdown = this.selectDropdown.bind(this);
    this.getDescription = this.getDescription.bind(this);
  }

  componentDidMount() {
    this.setState({
      categories: [
        {
          name: "BMW",
          subcategories: ["Series 1", "Series 2", "Series 3"],
          description:
          "https://www.bmw.in/content/dam/bmw/marketIN/bmw_in/all-models/x-series/X4/2021/X4_Desk_1680x756_2_1.jpg/jcr:content/renditions/cq5dam.resized.img.1680.large.time1646894257038.jpg"
        },
        {
          name: "HYUNDAI",
          subcategories: ["I10", "I20", "I20 Active"],
          description:
          "https://www.drivespark.com/car-image/640x480x100/car/37743090-hyundai_alcazar.jpg"
        },
        {
          name: "TATA",
          subcategories: ["Nexon", "Alto"],
          description:
          "https://cars.tatamotors.com/images/nexon/nexon-royale-blue-desktop-banner.jpg"
        },
        {
          name: "MARUTI SUZUKI",
          subcategories: [
            "BALENO",
            "WAGON-R"
          ],
          description:
          "https://cdni.autocarindia.com/utils/imageresizer.ashx?n=http://cms.haymarketindia.net/model/uploads/modelimages/S-PressoModelImage.jpg&w=872&h=578&q=75&c=1"
        }
      ]
    });
  }

  selectDropdown(e) {
    //Category
    this.setState({ selectdropdown: e.target.value });
    const category = this.state.categories.find(
      cat => cat.name === e.target.value
    );
    this.setState({ subcategories: category.subcategories });
    this.setState({ catObj: category });
  }

  getDescription(e) {
    this.setState({ description: this.state.catObj.description });
  }

  render() {
    return (
      <div>
        <MDBContainer>
          <MDBRow>
            <MDBCol>
              <h3 className="w-100 m-auto pt-1 pb-4">Select the car manufacture and series</h3>
              <select onChange={this.selectDropdown}>
                {/* Binding categories to select */}
                <option>--Select Car Manufacture--</option>
                {this.state.categories.map((x, key) => {
                  return <option key={key}>{x.name}</option>;
                })}
              </select>

              <select onChange={this.getDescription}>
                <option selected disabled>--Select Car Series--</option>
                {this.state.subcategories.map((x, key) => {
                  return <option key={key}>{x}</option>;
                })}
              </select>
              <br />

              <img className="description text-center" src = {this.state.description} />
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </div>
    );
  }
}
