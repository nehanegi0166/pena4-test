import './App.css';
import CASDropdown from './CASDropdown';

function App() {
  return (
    <div className="App">
      <CASDropdown />
    </div>
  );
}

export default App;
